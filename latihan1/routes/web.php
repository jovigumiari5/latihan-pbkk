<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\FoodController;
use App\Http\Controllers\TravelController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('biodata');
});

Route::get('/',[PageController::class, 'home']);
Route::get('/profile',[PageController::class, 'profile']);
Route::get('/product',[PageController::class, 'product']);
Route::get('/travel',[PageController::class, 'travel']);
Route::get('/food',[PageController::class, 'food']);
Route::get('/hitung',[PageController::class, 'hitung']);

Route::group(['prefix' => '/news'],
    function(){
    Route::get('/', [NewsController::class, 'index']);
    Route::get('/add', [NewsController::class, 'create']);
    Route::post('/create', [NewsController::class, 'save']);
    Route::get('/delete/{id}', [NewsController::class, 'delete']);
    Route::get('/edit/{id}', [NewsController::class, 'edit']);
    Route::post('/update', [NewsController::class, 'update']);
    Route::get('/detail/{id}', [NewsController::class, 'detail']);  
});

Route::group(['prefix' => '/food'],
    function(){
    Route::get('/', [FoodController::class, 'index']);
    Route::get('/add', [FoodController::class, 'create']);
    Route::post('/create', [FoodController::class, 'save']);
    Route::get('/delete/{id}', [FoodController::class, 'delete']);
    Route::get('/edit/{id}', [FoodController::class, 'edit']);
    Route::post('/update', [FoodController::class, 'update']);
});

Route::group(['prefix' => '/travel'],
    function(){
    Route::get('/', [TravelController::class, 'index']);
    Route::get('/add', [TravelController::class, 'create']);
    Route::post('/create', [TravelController::class, 'save']);
    Route::get('/delete/{id}', [TravelController::class, 'delete']);
    Route::get('/edit/{id}', [TravelController::class, 'edit']);
    Route::post('/update', [TravelController::class, 'update']);
    
});

Route::group(['prefix' => '/category'],
    function(){
    Route::get('/', [CategoryController::class, 'index']);
    Route::get('/add', [CategoryController::class, 'create']);
    Route::post('/create', [CategoryController::class, 'save']);
    Route::get('/delete/{id}', [CategoryController::class, 'delete']);
    Route::get('/edit/{id}', [CategoryController::class, 'edit']);
    Route::post('/update', [CategoryController::class, 'update']);
});

Route::group(['prefix' => '/product'],
    function(){
    Route::get('/', [ProductController::class, 'index']);
    Route::get('/add', [ProductController::class, 'create']);
    Route::post('/create', [ProductController::class, 'save']);
    Route::get('/delete/{id}', [ProductController::class, 'delete']);
    Route::get('/edit/{id}', [ProductController::class, 'edit']);
    Route::post('/update', [ProductController::class, 'update']);
});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
