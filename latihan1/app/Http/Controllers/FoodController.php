<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\models\Food;
use App\models\Category;

class FoodController extends Controller
{
    public function index(){
        $data = Food::with('category')->orderBy('id','desc')->get();
        return view('food.index',['data' => $data]);
    }
    
    public function create(){
        $category = Category::get();
        return view('food.create',['category' => $category]);
    }
    
    public function save(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|string||max:255',
            'price' => 'required|numeric'
        ]);
        
        if($validator->fails()){
            return redirect('/food/add')
            ->withErrors($validator)
            ->withInput();
        }else{
            $file = $request->foto;

            $food = New Food();
            $food->name = $request->name;
            $food->category_id = $request->name;
            $food->price = $request->price;
            $food->description = $request->description;
            $food->image = url('images/',$request->name.".".$file->getClientOriginalExtension());
            $food->save();

            $file->move('images/',$request->name.".".
            $file->getClientOriginalExtension());
            return redirect('/food');
        }
    }
    
    public function delete($id){
        $food = Food::where('id',$id)->delete();
        return redirect('/food');
    }
    
    public function edit($id){
        $category = Category::get();
        $food = Food::where('id',$id)->first();
        return view('food.edit',['food' => $food,'category' => $category]);
    }
    
    public function update(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'price' => 'required|numeric'
        ]);
        
        if($validator->fails()){
            return redirect('/food/edit/'.$request->id)
            ->withErrors($validator)
            ->withInput();
        }else{
            $file = $request->foto;

            $food = Food::where('id',$request->id)->update(
                [
                    'name' => $request->name,
                    'category' => $request->category_id,
                    'price' => $request->price,
                    'image' => url('images/',$request->name.".".$file->getClientOriginalExtension()),
                    'description' => $request->description,
                    
                    ]
                );
                $file->move('images/',$request->name.".".
                $file->getClientOriginalExtension());
                return redirect('/food');
            }
        }
    }
    