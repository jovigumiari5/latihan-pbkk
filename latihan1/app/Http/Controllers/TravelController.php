<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Travel;

class TravelController extends Controller
{
    public function index(){
        $data = Travel::get();
        return view('travel.index', ['data' => $data]);
    }

    public function create(){
        return view('travel.create');
    }

    public function save(Request $request){
        $travel = New Travel();
        $travel->title = $request->title;
        $travel->price = $request->price;
        $travel->description = $request->description;
        $travel->save();
        return redirect('/travel');
    }

    public function delete($id){
        $travel = Travel::where('id',$id)->delete();
        return redirect('/travel');
    }

    public function edit($id){
        $travel = Travel::where('id',$id)->first();
        return view('travel.edit',['travel' => $travel]);
    }

    public function update(Request $request){
        $travel = Travel::where('id',$request->id)->update(
            [
                'title' => $request->title,
                'price' => $request->price,
                'description' => $request->description,

            ]
        );
        return redirect('/travel');
       
    }

}
