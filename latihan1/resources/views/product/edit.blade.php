@extends('layouts.app')
@section('title','Halaman Product')
@section('main')

<div class="container">
    <div class="row mt-3 mb-3">
        <form action="{{ url('/product/update') }}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{ $product->id }}">
        <div class="mb-3">
            <label>Nama</label>
            <input type="text" class="form-control" name="name" value="{{ $product->name }}">
        </div>
        <div class="mb-3">
            <label>Harga</label>
            <input type="number" class="form-control" name="price" value="{{ $product->price }}">
        </div>
        <div class="mb-3">
            <label>Deskripsi</label>
            <textarea name="description" class="form-control">{{ $product->description }}</textarea>
        </div>
        <div class="mb-3">
            <button class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>
</div>

@endsection