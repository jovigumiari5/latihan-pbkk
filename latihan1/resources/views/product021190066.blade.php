@extends('layouts.app')
@section('title','Product')

@section('main')

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Product</title>
</head>
<body>
 <p> Orang tua saya tinggal di daerah pegunungan yang airnya berlimpah. Praktis, di sana tidak dibutuhkan mesin pompa karena air dialirkan begitu saja dari tempat yang tinggi ke setiap rumah. Ketika ibu saya mulai bekerja dan jadi kekurangan waktu, beliau kerap tidak sempat mencuci pakaian. Bahkan, menyetrika pakaian pun sering terabaikan.


Melihat kondisi ibu, saya tergugah untuk membelikan beliau mesin cuci manual yang cocok dengan karakter lingkungan dan sumber daya di rumah. Saya memutuskan untuk membeli mesin cuci dua tabung merek Sanken tipe TW-9770BU. Di samping pertimbangan harganya yang terjangkau, saya memilih produk ini karena mampu menghemat penggunaan listrik dan air. Sangat cocok dengan kondisi di rumah ibu saya yang daya listriknya hanya 900 watt serta tidak menggunakan mesin pompa dan tandon air.


Sejak ada mesin cuci Sanken Twin Tube ini, ibu tak lagi sering menumpuk pakaian kotor. Setiap kali kami pulang kampung mengunjungi beliau, saya pun bisa mencuci pakaian anak-anak setiap hari. Saya jadi tidak perlu repot lagi menyediakan wadah khusus pakaian kotor untuk dibawa pulang dan dicuci di rumah.</p>
<p>
 <img src="mesin.jpg" width="500" height="700">
</P>
</body>
</html>

@endsection