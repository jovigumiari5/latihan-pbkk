@extends('layouts.app')
@section('title','Halaman Travel')
@section('main')

<div class="container">
    <div class="row mt-3 mb-3">
        <a class="btn btn-primary mb-2" href="{{ url('/travel/add') }}
        ">Tambah Data</a>
        @foreach($data as $travel)
        <div class="col-3">
            <div class="card">
            <div class="card-header">
            <b>{{ $travel->title }}</b> (Rp  <i>{{ number_format($travel->price,2,",",".") }}</i>)
            </div>
            <div class="card-body">
                {{ $travel->description }}
            </div>
            <div class="card-footer">
            <a href="{{ url('/travel/edit/'.$travel->id) }}" class="btn
              btn-warning btn-sm">Edit</a>
              <a href="{{ url('/travel/delete/'.$travel->id) }}" class="btn
              btn-danger btn-sm">Hapus</a>
            </div>
        </div>
        </div>
        @endforeach
    </div>
</div>

@endsection