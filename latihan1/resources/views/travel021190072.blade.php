@extends('layout.app')
@section('title','Food')

@section('main')
    
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="UTF-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <meta http-equiv="X-UA-Compatible" content="ie=edge"> 
    <title>Travels</title> 
</head> 
<body> 
    <h2>Pantai Parai Tenggiri (Bangka Belitung)</h2> 
    <table> 
    <tbody> 
    <tr> 
        <td>Parai Tenggiri memiliki struktur pantai yang landai dengan air laut berwarna hijau toska serta pasir putihnya yang lembut. 
            Ombak di pantai ini juga tenang sehingga menjadi salah satu alasan yang menarik bagi wisatawan yang senang berenang.
            Tidak hanya berenang, kamu juga bisa menikmati aktivitas memancing, parasailing, menyelam, snorkeling, dan masih banyak lainnya.</td>

       
    </tr> 
    
</body> 
 
</html>

@endsection