@extends('layouts.app')
@section('title','Halaman Food')
@section('main')
@guest
<div class="container">
    <div class="row mt-3 mb-3">
        @foreach($data as $food)
        <div class="col-3">
            <div class="card">
            <div class="card-header">
                <b>{{ $food->name }}</b> (Rp  <i>{{ number_format($food->price,2,",",".") }}</i>)
            </div>
            <div class="card-body">
                {{ $food->description }}
            </div>
        </div>
        </div>
        @endforeach
    </div>
</div>
@else
<div class="container">
    <div class="row mt-3 mb-3">
        <a class="btn btn-primary mb-2" href="{{ url('/food/add') }}
        ">Tambah Data</a>
        @foreach($data as $food)
        <div class="col-3">
            <div class="card">
            <div class="card-header">
                <b>{{ $food->name }}</b> (Rp  <i>{{ number_format($food->price,2,",",".") }}</i>)
            </div>
            <div class="card-body">
                <img src="{{ $food->image }}" alt="{{ $food->name }}" width="100"><br>
                {{ $food->description }}
                Kategori: {{ $food->category->name }}
            </div>
            <div class="card-footer">
            <a href="{{ url('/food/edit/'.$food->id) }}" class="btn
              btn-warning btn-sm">Edit</a>
              <a href="{{ url('/food/delete/'.$food->id) }}" class="btn
              btn-danger btn-sm">Hapus</a>
            </div>
        </div>
        </div>
        @endforeach
    </div>
</div>
@endguest
@endsection