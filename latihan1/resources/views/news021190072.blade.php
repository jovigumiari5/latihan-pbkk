@extends('layout.app')
@section('title','Food')

@section('main')


<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="UTF-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <meta http-equiv="X-UA-Compatible" content="ie=edge"> 
    <title>News</title> 
</head> 
<body> 
    <h2> News </h2> 
    <table> 
    <tbody> 
    <tr> 
        <td>Samarinda, CNN Indonesia -- Area Sungai Tempadung atau Teluk Balikpapan disebut-sebut terancam aktivitas pembangunan fasilitas pengolahan dan pemurnian mineral (smelter) nikel di kawasan Industri Kariangau (KIK), Kelurahan Kariangau, Kecamatan Balikpapan Barat, Kalimantan Timur.
"Kami telah melakukan tinjauan ke lapangan dan menemukan adanya perusakan kawasan mangrove," ujar Husen Suwarno, Koordinitor Advokasi Pokja Pesisir dan Nelayan Balikpapan kepada CNNIndonesia.com akhir bulan lalu.
Lebih lanjut dia menerangkan, setidaknya ada 30 hektare (ha) lahan mangrove yang dibabat. Detailnya, sebanyak 10 ha di pesisir sungai dan 20 ha lagi berada di kawasan darat. Kuat dugaan pembalakan tersebut dimulai sebelum Desember 2021 dan berlangsung hingga Maret 2022.
"Kami kaget saja tak ada pemberitahuan dengan papan pengerjaan proyek. Dan kami telusuri perusahaan tersebut belum punya izin dan dokumen amdal," terangnya.
</td> 
        
    </tr> 
    
</body> 
 
</html>

@endsection